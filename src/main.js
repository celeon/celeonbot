require('dotenv').config();
const fetch = require('node-fetch');
const EventSource = require('eventsource');

const Discord = require('discord.js');
const client = new Discord.Client();

const sendMessages = (messages) => {
  messages.map((message) => {
    const channel = client.channels.find(val => val.name === message.channel);
    if (channel) {
      channel.send(message.content).then(() => {
        fetch(`${process.env.API_BASE_URL}/api/messages/${message.id}`, {
          method: 'patch',
          body: JSON.stringify({ isSent: true }),
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/merge-patch+json'
          }
        }).then(response => response.json())
          .then(json => console.log(json));
      });
    }
  });
};

client.once('ready', () => {
  console.log('Client ready !');
  client.guilds.map(guild => {
    const e = new EventSource(`${process.env.MERCURE_BASE_URL}?topic=${encodeURIComponent('http://example.com/messages')}`);
    e.onmessage = event => {
      if (event.data) {
        console.log('Data received');
        sendMessages(JSON.parse(event.data));
      }
    };
  });
});

client.login(process.env.DISCORD_BOT_TOKEN);
